# ddfix

https://www.ttlg.com/forums/showthread.php?t=121449

Based on ddfix 1.3.11 (http://timeslip.users.sourceforge.net/current/ddfixSource.7z) by Timeslip
(http://timeslip.users.sourceforge.net/).

This repository retroactively records the version history of ddfix since version 1.3.11.

ddfix is mostly obsolete. Use NewDark (https://www.ttlg.com/forums/showthread.php?t=150989)
instead.
